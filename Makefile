NAME := rtv1
NPWD := $(CURDIR)/$(NAME)

CC := gcc -march=native -mtune=native -flto -Ofast -pipe -funroll-loops
CC_DEBUG := gcc -g
CFLAGS := 
IFLAGS := -I $(CURDIR)/includes/ -I $(CURDIR)/utils/libft/includes
LIBSINC :=
LIBS :=
GNL_DIR = $(addprefix ./utils/gnl/, $(GNL))
GNL = get_next_line.c
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	LIBSINC += -I ~/.brew/include
	LIBS += -L ~/.brew/lib -rpath ~/.brew/lib
endif
LIBS += -F ~/Library/Frameworks -framework SDL2 -F ~/Library/Frameworks/

SRC := srcs/main.c \
		srcs/render/render.c \
		srcs/render/intersect.c \
		srcs/render/image.c \
		srcs/render/color.c \
		srcs/render/vec3.c \
		srcs/render/object.c \
		srcs/shapes/shape_inter.c \
		srcs/controls/controls.c \
		srcs/controls/key.c \
		srcs/controls/mouse.c \
		srcs/init.c \
		srcs/parser/reader.c \
		srcs/parser/parse.c \
		srcs/help.c

OBJ := $(SRC:.c=.o)
OBJS = $(addprefix $(OBJ_DIR)/,$(SRCS:.c=.o))
OBJ_DIR = objs

LIBFT := $(CURDIR)/utils/libft/libft.a
LMAKE := make -sC utils/libft

DEL := rm -rf

WHITE := \033[0m
BGREEN := \033[42m
GREEN := \033[32m
RED := \033[31m
INVERT := \033[7m

SUCCESS := [$(GREEN)✓$(WHITE)]
SUCCESS2 := [$(INVERT)$(GREEN)✓$(WHITE)]

all: $(NAME);

$(OBJ): %.o: %.c
	@echo -n ' $@: '
	@$(CC) -c $(CFLAGS) $(LIBSINC) $(IFLAGS) $< -o $@
	@echo "$(SUCCESS)"

install:
	@if [ ! -f ~/.brewconfig.zsh ]; \
	then \
		curl -fsSL https://rawgit.com/kube/42homebrew/master/install.sh | zsh; \
		brew install sdl2; \
	fi;
	
$(LIBFT):
	@$(LMAKE)

$(NAME): install $(LIBFT) $(OBJ)
	@echo -n ' <q.p> | $(NPWD): '
	@$(CC) $(OBJ) $(LIBS) $(GNL_DIR) $(LIBFT) -o $(NAME)
	@mkdir -p $(OBJ_DIR)
	@echo "$(SUCCESS2)"

del:
	@$(DEL) $(OBJ)
	@$(DEL) $(NAME)

pre: del all
	@echo "$(INVERT)$(GREEN)Successed re-build.$(WHITE)"

debug:
	$(CC) $(SRC) $(LIBS) $(GNL_DIR) $(LIBFT) $(IFLAGS) -g -o $(NAME)

$(CC_DEBUG):
	@$(eval CC=$(CC_DEBUG))
	debug_all: $(CC_DEBUG) pre
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(WHITE)"
	debug: $(CC_DEBUG) all
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(WHITE)"

clean:
	@$(DEL) $(OBJ) $(OBJ_DIR)
	@$(LMAKE) clean

fclean: clean
	@$(LMAKE) fclean
	@$(DEL) $(NAME)
	@echo "$(INVERT)$(RED)deleted$(WHITE)$(INVERT): $(NPWD)$(WHITE)"

re: fclean all

.PHONY: all fclean clean re pre debug debug_all set_cc_debug install
