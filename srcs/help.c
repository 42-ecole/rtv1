#include "../includes/rtv1.h"

int ft_rm_char(char *dest, char c)
{
   int removed = 0;
   char *tmp;

   while (*dest)
   {
	   tmp = ft_strchr(dest,c);
	   if (NULL==tmp)
		   break;
	   size_t len = ft_strlen(tmp+1);
	   ft_memmove(tmp,tmp+1,len);
	   tmp[len] = 0;
	   ++removed;
	   dest=tmp;
   }
   return (removed);
}

char    *ft_strrchr1(const char *s, int c)
{
    int             i;
 
    i = 0;
    while (s[i] != '\0')
        i++;
    while (i >= 0)
    {
        if (s[i] == c)
            return (char*)(s + i);
        i--;
    }
    return (NULL);
}

t_vec3       get_vector3(char *str)
{
	t_vec3   vec;
	char    **vector_arr;
 
	ft_rm_char(str, '\n');
	str = ft_strrchr1(str++, '(');
	str++;
	vector_arr = ft_strsplit(str, ' ');
	vec = vec_3(ft_atoi(vector_arr[0]), ft_atoi(vector_arr[1]), ft_atoi(vector_arr[2]));
	return (vec);
}
