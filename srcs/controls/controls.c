/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 12:28:54 by rfunk             #+#    #+#             */
/*   Updated: 2019/11/27 18:20:10 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void	rotate_obj(t_window *w, t_object *obj)
{
	if (w->controls.rotleft)
	{
		obj->world.fwd = rotate_y(obj->world.fwd, ROTSPEED_P);
		obj->world.right = normalize(cross_v(obj->world.fwd, obj->world.up));
	}
	if (w->controls.rotright)
	{
		obj->world.fwd = rotate_y(obj->world.fwd, ROTSPEED_M);
		obj->world.right = normalize(cross_v(obj->world.fwd, obj->world.up));
	}
	if (w->controls.rotup)
	{
		obj->world.fwd = rotate_x(obj->world.fwd, ROTSPEED_P);
		obj->world.up = normalize(cross_v(obj->world.right, obj->world.fwd));
	}

	if (w->controls.rotdown)
	{
		obj->world.fwd = rotate_x(obj->world.fwd, ROTSPEED_M);
		obj->world.up = normalize(cross_v(obj->world.right, obj->world.fwd));
	}
}

void	move_obj(t_window *w, t_object *obj)
{
	if (w->controls.w)
	{
		obj->world.fwd = (cross_v(obj->world.up, obj->world.right));
		obj->transform.position = sum_v(obj->transform.position, multiply_by_scalar(obj->world.fwd, MOVESPEED));
	}
	if (w->controls.s)
	{
		obj->world.fwd = (cross_v(obj->world.up, obj->world.right));
		obj->transform.position = sub_v(obj->transform.position, multiply_by_scalar(obj->world.fwd, MOVESPEED));

	}
	if (w->controls.d)
	{
		obj->world.right = (cross_v(obj->world.fwd, obj->world.up));
		obj->transform.position = sum_v(obj->transform.position, multiply_by_scalar(obj->world.right, MOVESPEED));
	}
	if (w->controls.a)
	{
		obj->world.right = (cross_v(obj->world.fwd, obj->world.up));
		obj->transform.position = sub_v(obj->transform.position, multiply_by_scalar(obj->world.right, MOVESPEED));
	}
	if (w->controls.space)
	{
		obj->world.up = (cross_v(obj->world.right, obj->world.fwd));
		obj->transform.position = sub_v(obj->transform.position, multiply_by_scalar(obj->world.up, MOVESPEED));
	}
	if (w->controls.c)
	{
		obj->world.up = (cross_v(obj->world.right, obj->world.fwd));
		obj->transform.position = sum_v(obj->transform.position, multiply_by_scalar(obj->world.up, MOVESPEED));
	}
//	printf("fwd: %f %f %f\n", obj->world.fwd.x, obj->world.fwd.y, obj->world.fwd.z);
//	printf("up: %f %f %f\n", obj->world.up.x, obj->world.up.y, obj->world.up.z);

}

void	handle_keys(t_window *w)
{
	t_object *obj;

	if (key_action(&w->e, SDLK_ESCAPE, SDL_KEYDOWN))
		w->power = false;
	if (key_action(&w->e, SDLK_EQUALS, SDL_KEYDOWN))
		w->lights[0]->brightness += 0.05;
	if (key_action(&w->e, SDLK_MINUS, SDL_KEYDOWN))
		w->lights[0]->brightness -= 0.05;
	// if (key_action(&w->e, SDLK_f, SDL_KEYDOWN))
	// 	w->app->objects[0].transform.position.x++;
	switch_key(w, SDLK_TAB, &w->temp_tr);
	if (w->temp_tr)
	{
		obj = find_obj_by_type(w->app, T_POINTLIGHT);
		w->lights[0] = obj;
	}
	else
		obj = &w->camera;
	if (w->e.type == SDL_KEYDOWN || w->e.type == SDL_KEYUP)
	{
		w->running = false;
		w->running = handle_key(w, SDLK_w, &(w->controls.w));
		w->running = handle_key(w, SDLK_a, &(w->controls.a));
		w->running = handle_key(w, SDLK_s, &(w->controls.s));
		w->running = handle_key(w, SDLK_d, &(w->controls.d));
		w->running = handle_key(w, SDLK_LEFT, &(w->controls.rotleft));
		w->running = handle_key(w, SDLK_RIGHT, &(w->controls.rotright));
		w->running = handle_key(w, SDLK_UP, &(w->controls.rotup));
		w->running = handle_key(w, SDLK_DOWN, &(w->controls.rotdown));
		w->running = handle_key(w, SDLK_SPACE, &(w->controls.space));
		w->running = handle_key(w, SDLK_c, &(w->controls.c));
		move_obj(w, obj);
		rotate_obj(w, obj);
		//printf("%f, %f, %f\n", obj->transform.position.x, obj->transform.position.y, obj->transform.position.z);
		if (w->running)
			life_cycle(w);
	}
	if (!w->running)
	{
	// mouse events
	if (w->e.type == SDL_MOUSEBUTTONDOWN)
		mouse_click(&(w->e.button), &(w->controls.mouse));
	if (w->e.type == SDL_MOUSEBUTTONUP)
	 	mouse_released(&(w->controls.mouse));
	if (w->e.type == SDL_MOUSEMOTION  && w->controls.mouse.clicked)
		mouse_move(&(w->controls.mouse), w->e.motion.xrel, w->e.motion.yrel, &(w->camera), w);
	if (w->e.type == SDL_MOUSEWHEEL)
		mouse_scroll(w->e.wheel.y,&(w->camera), w);
	}
}

void	event_cycle(t_window *w)
{
	while (SDL_PollEvent(&(w->e)))
	{
		if (w->e.type == SDL_WINDOWEVENT)
			if (w->e.window.event == SDL_WINDOWEVENT_CLOSE)
				w->power = false;
		handle_keys(w);
	}
}
