/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 14:54:38 by rfunk             #+#    #+#             */
/*   Updated: 2019/11/27 18:17:50 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

bool	save_mouse_pos(t_mouse *mouse)
{
	mouse->clicked = true;
	SDL_GetMouseState(&(mouse->x), &(mouse->y));
	return (true);
}

void	mouse_click(SDL_MouseButtonEvent* b, t_mouse *mouse)
{
	if(b->button == 1)
		save_mouse_pos(mouse);
	mouse->start_x = mouse->x;
	mouse->start_y = mouse->y;
}

void	mouse_released(t_mouse *mouse)
{
	mouse->clicked = false;
	mouse->x = 0;
	mouse->y = 0;
	mouse->start_x = 0;
	mouse->start_y = 0;
}

void	mouse_move(t_mouse *mouse, int relx, int rely, t_object *obj, t_window *w)
{
	// rotation for camera
	float x_angle, y_angle;
		
	x_angle = 0.0001 * (mouse->start_x - mouse->x - (relx));
	y_angle = 0.0001 * (mouse->start_y - mouse->y - (rely));
	mouse->x = (mouse->start_x - mouse->x - (relx));
	mouse->y = (mouse->start_y - mouse->y - (rely));
	save_mouse_pos(mouse);

	obj->world.fwd = rotate_y(obj->world.fwd, x_angle);
	obj->world.right = normalize(cross_v(obj->world.fwd, obj->world.up));

	obj->world.fwd = rotate_x(obj->world.fwd, y_angle);
	obj->world.up = normalize(cross_v(obj->world.right, obj->world.fwd));
	life_cycle(w);
}

void	mouse_scroll(float value, t_object *obj, t_window *w)
{
	obj->world.fwd = (cross_v(obj->world.up, obj->world.right));
	obj->transform.position = sum_v(obj->transform.position, multiply_by_scalar(obj->world.fwd, -value));
	life_cycle(w);
}