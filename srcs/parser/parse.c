#include "../../includes/rtv1.h"

t_objtype	get_objecttype(t_list *lst)
{
	t_objtype	object_type;

	object_type = -1;
	if (ft_strstr(lst->content, "T_CAMERA"))
		object_type = T_CAMERA;
	else if (ft_strstr(lst->content, "T_PLANE"))
		object_type = T_PLANE;
	else if (ft_strstr(lst->content, "T_TRIANGLE"))
		object_type = T_TRIANGLE;
	else if (ft_strstr(lst->content, "T_CUBE"))
		object_type = T_CUBE;
	else if (ft_strstr(lst->content, "T_SPHERE"))
		object_type = T_SPHERE;
	else if (ft_strstr(lst->content, "T_CONE"))
		object_type = T_CONE;
	else if (ft_strstr(lst->content, "T_CYLINDER"))
		object_type = T_CYLINDER;
	else if (ft_strstr(lst->content, "T_PYRAMID"))
		object_type = T_PYRAMID;
	else if (ft_strstr(lst->content, "T_POINTLIGHT"))
		object_type = T_POINTLIGHT;

	return (object_type);
}

float			get_value(char *str)
{
	float	value;

	ft_rm_char(str, '\n');
	str = ft_strrchr(str, '=');
	str++;
	value = ft_atoi(str);
	return (value);
}

unsigned int	HexStringToUInt(char const* hexstring)
{
	unsigned int    result;
	char const      *str;
	unsigned int    add;
	char            c;

	result = 0;
	str = hexstring;
	while(*str)
	{
		c = *str;
		result <<= 4;
		if (c >= '0' &&  c <= '9')
			add = c - '0';
		else if (c >= 'A' && c <= 'F')
			add = c - 'A' + 10;
		else if (c >= 'a' && c <= 'f')
			add = c - 'a' + 10;
		else
			return (0x000000);
		result += add;
		++str;
	}
	return (result);  
}

unsigned int	get_colour(char *str)
{
	unsigned int   colour_hex;

	ft_rm_char(str, '\n');
	str = ft_strrchr(str, 'x');
	ft_rm_char(str, 'x');
	ft_rm_char(str, ';');
	colour_hex = HexStringToUInt(str);
	return (colour_hex);
}

int		init_orientation(t_object *obj)
{
	obj->world.look_at = vec_3(0, 0, 1.);
	obj->world.fwd = normalize(sub_v(obj->world.look_at, obj->transform.position));
	obj->world.up = normalize(vec_3(.0, 1., .0));
	obj->world.right = normalize(cross_v(obj->world.fwd, obj->world.up));
	return (0);
}

t_object		get_objectinfo(t_list *lst, t_window *w)
{
	t_object	obj;

	ft_rm_char(lst->content, ' ');
	obj.object_type = get_objecttype(lst);
	while (lst)
	{
		ft_rm_char(lst->content, ' ');
		if (ft_strstr(lst->content, "{"))
		{
			lst = lst->next;
			while (lst)
			{
				if (ft_strstr(lst->content, "position = "))
				{
					if (obj.object_type == T_TRIANGLE)
					{
						obj.tr.a = get_vector3(lst->content);
						lst = lst->next;
						obj.tr.b = get_vector3(lst->content);
						lst = lst->next;
						obj.tr.c = get_vector3(lst->content);
					}
					else
						obj.transform.position = get_vector3(lst->content);
				}
				else if (ft_strstr(lst->content, "rotation = "))
					obj.transform.rotation = get_vector3(lst->content);
				else if (ft_strstr(lst->content, "color = "))
				{
					obj.color = get_colour(lst->content);
					obj.vcolor = hex_to_vec3(obj.color);
				}
				else if (ft_strstr(lst->content, "width = "))
					obj.shape.width = get_value(lst->content);
				else if (ft_strstr(lst->content, "height = "))
					obj.shape.height = get_value(lst->content);
				else if (ft_strstr(lst->content, "depth = "))
					obj.shape.depth = get_value(lst->content);
				else if (ft_strstr(lst->content, "radius = "))
				{
					obj.shape.radius = get_value(lst->content);
				}
				else if (ft_strstr(lst->content, "}"))
				{
					lst = lst->next;
					return (obj);
				}
				lst = lst->next;
			}			
		}
		init_orientation(&obj);
		if (obj.object_type == T_POINTLIGHT)
		{
			w->lights[0] = &obj;
			w->lights[0]->brightness = 10000;
		}
		lst = lst->next;
	}
	return (obj);
}

void	parse_list(t_list *lst, t_window *w)
{
	t_list	*tmp_lst;	
	int		object_counter;

	tmp_lst = lst;
	ft_rm_char(lst->content, '\n');
	ft_rm_char(lst->content, ' ');
	object_counter = 0;
	while (tmp_lst)
	{
		if (ft_strstr(tmp_lst->content, "OBJECT:"))
			object_counter++;
		tmp_lst = tmp_lst->next;
	}
	w->app->objs_count = object_counter;
	w->app->objects = (t_object*)malloc(sizeof(t_object) * (object_counter));
	object_counter = 0;
	while (lst)
	{
		if (ft_strstr(lst->content, "OBJECT:"))
		{
			w->app->objects[object_counter++] = get_objectinfo(lst, w);
			
		}
		lst = lst->next;
	}
}
