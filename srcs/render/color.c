/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 20:12:38 by rfunk             #+#    #+#             */
/*   Updated: 2019/09/25 19:06:42 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

Uint32		set_color(t_rgb rgb)
{
	return (
			((((int)rgb.r) << 16) & 0xff0000) |
			((((int)rgb.g) << 8) & 0xff00) |
			((int)rgb.b));
}

Uint32		form_color(int r, int g, int b)
{
	return (
			((r << 16) & 0xff0000) |
			((g << 8) & 0xff00) |
			(b));
}
