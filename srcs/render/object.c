/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/17 17:20:27 by rfunk             #+#    #+#             */
/*   Updated: 2019/11/14 20:03:30 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/rtv1.h"

t_object    *find_obj_by_type(t_app *app, int value)
{
    int i;

    i = 0;
    while (i < app->objs_count)
    {
        if (app->objects[i].object_type == value)
            return (&(app->objects[i]));
        i++;
    }
}