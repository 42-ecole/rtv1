/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersect.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 16:56:02 by rfunk             #+#    #+#             */
/*   Updated: 2019/11/14 20:14:55 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/rtv1.h"

t_vec3	light_shader(t_window *w, t_object *obj, float dist, t_object **lights)
{
	float	diffuse_light;
	t_vec3	light_dir;
	t_vec3		orig;
	t_vec3		dir;
	t_vec3		ret;
	float		light;

	orig = w->camera.transform.position;
	dir = w->camera.transform.direction;
	diffuse_light = 0;
	light_dir = normalize(sub_v(lights[0]->transform.position, obj->hit));
	light = lights[0]->brightness / powf((dist), 2);
	diffuse_light =  light * fmaxf(0.f, dot_pr(light_dir, obj->normal));
	ret = multiply_by_scalar(obj->vcolor, diffuse_light);
	// float temp;
	// float i;
	// temp = dot_pr(normalize(obj->normal), light_dir);
	// i = 0;
	// if (temp > 0)
	// {
	// 	i += (temp * lights[0]->brightness) / (length(obj->normal) * length(light_dir));
	// }
	//ret = multiply_by_scalar(obj->vcolor, i);
	return (ret);
}

bool	calc_hit_normal(t_window *w, t_object **obj, t_vec3 orig, t_vec3 dir)
{
	(*obj)->hit = sum_v(orig, multiply_by_scalar(dir, (*obj)->distance));
	if ((*obj)->object_type == T_TRIANGLE)
	{
		(*obj)->normal = normalize(cross_v(sub_v((*obj)->tr.b, (*obj)->tr.a),
										sub_v((*obj)->tr.c, (*obj)->tr.b)));
		if (dot_pr(dir, (*obj)->normal) > 0)
			(*obj)->normal = multiply_by_scalar((*obj)->normal, -1);
	}
	else if ((*obj)->object_type == T_SPHERE || (*obj)->object_type == T_POINTLIGHT)
		(*obj)->normal = normalize(sub_v((*obj)->hit, (*obj)->transform.position));
	return (true);
}

bool	cast_rays(t_window *w, t_object *objs, int objcount, t_object *ret)
{
	// make sublist for each object type . cast_ray()
	float		max_dist;
	bool 		(*render_func)(t_vec3 orig, t_vec3 dir, t_object *obj);
	int			i;
	t_vec3		orig;
	t_vec3		dir;
	t_object	close_obj;

	i = 0;
	orig = w->camera.transform.position;
	dir = w->camera.transform.direction;
	max_dist = FLT_MAX;
	while (i < objcount)
	{
		if (objs[i].object_type == T_SPHERE)
			render_func = sphere_intersect;
		else if (objs[i].object_type == T_TRIANGLE)
			render_func = triangle_intersect;
		else if (objs[i].object_type == T_POINTLIGHT)
			render_func = point_intersect;
		else if (objs[i].object_type == T_PLANE)
			render_func = plane_intersect;
		if ((render_func(orig, dir, &objs[i]) && (objs[i].distance < max_dist)))
		{
			max_dist = objs[i].distance;
			*ret = objs[i];
		}
		i++;
		// add material in shpere struct
	}
	calc_hit_normal(w, &ret, orig, dir);

	if (!ret)
		return (false);
	if (max_dist == FLT_MAX)
		return (false);
	return (true);
}
