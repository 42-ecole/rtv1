/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 21:57:30 by rfunk             #+#    #+#             */
/*   Updated: 2019/10/17 18:27:18 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/rtv1.h"

bool	object_handler(t_window *w,t_object *objs, t_object **lights, Uint32 i, Uint32 j, int obj_count, t_vec3 *ret)
{
	double	focus_x;
	int		k;
	float	max_dist;
	t_object	render_obj;

	k = 0;
	max_dist = FLT_MAX;
	w->camera.world.fwd = normalize(w->camera.world.fwd);
	focus_x = tan(1.5708 - FOV / 2) * WIDTH / 2;
	t_vec3 image_point = sum_v(sum_v(multiply_by_scalar(w->camera.world.right, i),
									multiply_by_scalar(w->camera.world.up, j)),
									multiply_by_scalar(w->camera.world.fwd, focus_x));
	w->camera.transform.direction = normalize(image_point);
	if (cast_rays(w, objs, obj_count, &render_obj))
	{
		//*ret = hex_to_vec3(0xffffff);
		*ret = light_shader(w, &render_obj, render_obj.distance ,lights);
		return (true);
	}
	*ret = w->background;
	return(false);
}

void	*region_render(void *w)
{
	t_window	*window;
	int i;
	int j;

	i = -1;
	window = (t_window *)w;
	window->frame_buf = (t_vec3 *)malloc(sizeof(t_vec3) * WIDTH * window->end_r);
	while (++i < WIDTH)
	{
		j = window->start_r - 1;
		while (++j < window->end_r)
			object_handler(window, window->app->objects, window->lights, i, j,
			window->app->objs_count, &(window->frame_buf[WIDTH * j +i]));
	}
	return (window);
}

void	render(t_window *w, void *f)
{
	pthread_t	thread[THREADS];
	t_window	region[THREADS];
	int			i;
	int			htr;

	i = -1;
	htr = HEIGHT/ THREADS;
	while (++i < THREADS)
	{
		ft_memcpy((void*)&region[i], (void *)w, sizeof(t_window));
		region[i].start_r = htr * i;
		region[i].end_r = htr * (i + 1);
		pthread_create(&thread[i], NULL, f, &region[i]);
	}
	while (i--)
	{
		pthread_join(thread[i], NULL);
		draw_framebuf(&region[i], region[i].frame_buf, (Uint32)WIDTH,
		(Uint32)(region[i].end_r),(Uint32)(region[i].start_r));
		SDL_UpdateWindowSurface(region[i].window);
		free(region[i].frame_buf);
	}
}
