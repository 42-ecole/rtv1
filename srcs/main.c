/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 18:52:21 by rfunk             #+#    #+#             */
/*   Updated: 2019/11/14 21:21:03 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void	draw_cycle(t_window *w)
{
	render(w, region_render);
}

void	life_cycle(t_window *w)
{
	//free(*(w->frame_buf));

	w->ptr = w->screen->pixels;
	w->background = hex_to_vec3(0x353535);
	draw_cycle(w);
}

int		main(int argc, char *argv[])
{
	t_window w;

	argc++;
	w.app = (t_app *)malloc(sizeof(t_app));
	init_lights(&w);
	parse_list(read_file(argv[1]), &w);
	global_init(&w);
	life_cycle(&w);
	while (w.power)
		event_cycle(&w);
	SDL_DestroyWindow(w.window);
	SDL_Quit();
	return (0);
}
